package com.meetup.javademo.dao;

import com.meetup.javademo.domain.ApplicationError;
import com.meetup.javademo.domain.Record;
import com.mongodb.client.result.DeleteResult;
import io.vavr.control.Either;
import io.vavr.control.Try;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RecordRepositoryCustomImpl implements RecordRepositoryCustom {

  private final MongoTemplate mongoTemplate;

  @Autowired
  public RecordRepositoryCustomImpl(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @Override
  public Either<ApplicationError, Record> findById(String id) {
    return Try.of(() -> Optional.ofNullable(mongoTemplate.findById(id, Record.class)))
        .toEither(new ApplicationError("MONGO_ERROR"))
        .flatMap(opt -> opt.map(this::right).orElse(this.left(new ApplicationError("NOTFOUND"))));
  }


  @Override
  public Either<ApplicationError, String> delete(Record record) {
    return Try.of(() -> mongoTemplate.remove(record))
        .toEither(new ApplicationError("MONGO_ERROR"))
        .flatMap(deleteResult -> rightDeleteResult(deleteResult, record.getId()));
  }

  @Override
  public Either<ApplicationError, Record> create(Record record) {
    return Try.of(() -> mongoTemplate.save(record))
        .toEither(new ApplicationError("MONGO_ERROR"));
  }

  private Either<ApplicationError, Record> left(ApplicationError t) {
    return Either.left(t);
  }

  private Either<ApplicationError, Record> right(Record r) {
    return Either.right(r);
  }

  private Either<ApplicationError, String> rightDeleteResult(DeleteResult deleteResult, String id) {
    return deleteResult.getDeletedCount() == 1 ? Either.right(id) : Either.left(new ApplicationError("DELETE_ERROR"));
  }
}
