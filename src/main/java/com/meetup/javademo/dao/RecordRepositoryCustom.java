package com.meetup.javademo.dao;

import com.meetup.javademo.domain.ApplicationError;
import com.meetup.javademo.domain.Record;
import io.vavr.control.Either;

public interface RecordRepositoryCustom {

  Either<ApplicationError, Record> findById(String s);

  Either<ApplicationError, String> delete(Record record);

  Either<ApplicationError, Record> create(Record record);

}
