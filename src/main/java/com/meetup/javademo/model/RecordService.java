package com.meetup.javademo.model;

import com.meetup.javademo.dao.RecordRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecordService {

  private final RecordRepositoryCustom recordRepository;

  @Autowired
  public RecordService(RecordRepositoryCustom recordRepository) {
    this.recordRepository = recordRepository;
  }

  // public String get(String id) {
  //
  //   Either<ApplicationError, Record> byId = recordRepository.findById(id);
  // }
}
