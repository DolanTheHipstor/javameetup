package com.meetup.javademo;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories
public class MeetupConfiguration {
  // private static final String LOGIN = "root";
  // private static final String PASSWORD = "password";

  @Bean
  public MongoTemplate mongoTemplate() {
    MongoClient mongoClient = mongoClient();
    return new MongoTemplate(mongoClient, "meetupDB");
  }

  @Bean
  public MongoClient mongoClient() {
    MongoCredential credential = MongoCredential.createCredential(null, "meetupDB", null);

    MongoClientSettings settings = MongoClientSettings.builder()
        .credential(credential)
        .applyConnectionString(new ConnectionString("mongodb://localhost:27017"))
        .build();

    return MongoClients.create(settings);
  }

}
