package com.meetup.javademo.domain;

import java.io.Serializable;
import java.util.StringJoiner;

public class ApplicationError implements Serializable {

  private String code;

  private String message;

  public ApplicationError(String code) {
    this.code = code;
    this.message = code;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", ApplicationError.class.getSimpleName() + "[", "]")
        .add("code='" + code + "'")
        .add("message='" + message + "'")
        .toString();
  }
}
