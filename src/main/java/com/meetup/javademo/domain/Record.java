package com.meetup.javademo.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.StringJoiner;
import org.springframework.data.annotation.Id;

public class Record implements Serializable {

  @Id
  private String id;

  private String recordMessage;

  private ZonedDateTime timeStamp;

  public Record(String recordMessage, ZonedDateTime timeStamp) {
    this.recordMessage = recordMessage;
    this.timeStamp = timeStamp;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getRecordMessage() {
    return recordMessage;
  }

  public void setRecordMessage(String recordMessage) {
    this.recordMessage = recordMessage;
  }

  public ZonedDateTime getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(ZonedDateTime timeStamp) {
    this.timeStamp = timeStamp;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Record.class.getSimpleName() + "[", "]")
        .add("id=" + id)
        .add("recordMessage='" + recordMessage + "'")
        .add("timeStamp=" + timeStamp)
        .toString();
  }
}
