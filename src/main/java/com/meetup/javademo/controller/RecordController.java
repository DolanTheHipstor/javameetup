package com.meetup.javademo.controller;

import com.meetup.javademo.dao.RecordRepositoryCustom;
import com.meetup.javademo.domain.ApplicationError;
import com.meetup.javademo.domain.Record;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RecordController {

  private final RecordRepositoryCustom recordRepository;

  @Autowired
  public RecordController(RecordRepositoryCustom recordRepository) {
    this.recordRepository = recordRepository;
  }

  @PostMapping(path = "/record", consumes = "application/json", produces = "application/json")
  public Either<ApplicationError, Record> create(@RequestBody Record record) {
    return recordRepository.create(record);
  }

  @GetMapping("/get/{id}")
  public Either<ApplicationError, Record> get(@PathVariable String id) {
    return recordRepository.findById(id);
  }
}
