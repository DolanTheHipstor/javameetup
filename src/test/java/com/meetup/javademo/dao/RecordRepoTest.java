package com.meetup.javademo.dao;

import com.meetup.javademo.MeetupTestConfig;
import com.meetup.javademo.dao.RecordRepoTest.RepositoryConfig;
import com.meetup.javademo.domain.ApplicationError;
import com.meetup.javademo.domain.Record;
import com.mongodb.MongoClient;
import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;
import io.vavr.control.Either;
import java.io.IOException;
import java.time.ZonedDateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RepositoryConfig.class})
@Import(MeetupTestConfig.class)
public class RecordRepoTest {

  @Autowired
  private RecordRepositoryCustom recordRepository;

  @Test
  public void test() {
    Record r = new Record("Record1", ZonedDateTime.now());
    Either<ApplicationError, Record> eitherRecord = recordRepository.create(r);
    eitherRecord.peek(Assert::assertNotNull);
  }

  @TestConfiguration
  static class RepositoryConfig {

    private static final String MONGO_DB_URL = "localhost";
    private static final String MONGO_DB_NAME = "embeded_db";

    @Bean
    public MongoTemplate mongoTemplate() throws IOException {
      EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
      mongo.setBindIp(MONGO_DB_URL);
      MongoClient client = mongo.getObject();
      MongoTemplate mongoTemplate = new MongoTemplate(client, MONGO_DB_NAME);
      return mongoTemplate;
    }

    @Bean
    RecordRepositoryCustom recordRepository() throws IOException {
      return new RecordRepositoryCustomImpl(mongoTemplate());
    }
  }

}
